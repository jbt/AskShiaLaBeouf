#include "RequestHandler.hpp"
#include <Poco/Net/HTTPServer.h>
#include <iostream>

namespace net = Poco::Net;

int main(int argc, char **argv)
{
    try
    {
        net::HTTPRequestHandlerFactory::Ptr factory{ new RequestHandlerFactory };
        net::ServerSocket socket{ std::uint16_t(80) };
        net::HTTPServer server{ factory, socket, new net::HTTPServerParams };
        server.start();
        std::clog << "Now wait.\n";
        ::sleep( 999999 );
        server.stop();
        return 0;
    }
    catch ( const std::exception& e )
    {
        std::clog << "FATAL ERROR: " << e.what() << '\n';
    }
    return 1;
}
