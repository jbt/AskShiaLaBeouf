#include "RequestHandler.hpp"
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/URI.h>
#include <cstdlib>
#include <iostream>
#include <fstream>

namespace net = Poco::Net;

namespace {
    std::vector<std::string> ReadFile()
    {
//         std::clog << __PRETTY_FUNCTION__ << '\n';
        std::ifstream answers{ "answers.txt" };
        std::string a;
        std::vector<std::string> rv;
        while ( std::getline(answers, a) )
        {
            if ( ! a.empty() )
            {
//                 std::clog << "READ: " << a << '\n';
                rv.push_back( a );
            }
        }
        return rv;
    }
    std::string FORM = "Ask Shia LaBeouf a question: <input type='text'/> <a href='ShiaSays'>Get Your Answer</a>";
}

Poco::Net::HTTPRequestHandler* RequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request)
{
    auto rv = new RequestHandler;
    rv->answer_ = Answer();
    std::clog << rv->answer_ << '\n';
    return rv;
}

void RequestHandler::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
    auto uri = request.getURI();
    std::clog << uri << '\n';
    if ( uri.find("ShiaSays") != std::string::npos )
    {
        response.send() << "<html><title>Shia LaBeouf Says:</title><h2>Shia LaBeouf Says:</h2><h1>"
            << answer_
            << "</h1><p>" << FORM << "</p></html>";
    }
    else
    {
        response.send() << "<html><title>Ask Shia LaBeouf</title>" << FORM << "</html>";
    }
}

RequestHandlerFactory::RequestHandlerFactory()
: answers_( ReadFile() ),
  rng_(),
  r_( 0, answers_.size() - 1 )
{
    std::random_device devrandom;
    rng_.seed( devrandom() );
}

std::string RequestHandlerFactory::Answer()
{
//     std::clog << __PRETTY_FUNCTION__ << '\n';
    auto i = r_( rng_ );
    if ( i >= answers_.size() )
    {
        std::clog << "\nRELOAD\n";
        auto reread = ReadFile();
        {
            std::unique_lock<std::mutex> lck( answers_mutex_ );
            answers_.insert( answers_.end(), reread.begin(), reread.end() );
        }
        return Answer();
    }
    auto it = std::next(answers_.begin(), i);
    auto rv = *it;
    {
        std::unique_lock<std::mutex> lck( answers_mutex_ );
        answers_.erase( it );
    }
    std::clog << i << ' ' << answers_.size() << '\n';
    return rv;
}
