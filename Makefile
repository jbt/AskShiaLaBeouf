srcs := $(wildcard *.cpp )
objs := $(addsuffix .o, $(srcs) ) 
libs := $(addprefix -l , $(addprefix Poco, Net Foundation ) ) 

askshialabeouf : $(objs) Makefile
	g++ -o $@ $(objs) $(libs) -g3 -O0

askshialabeouf.opt : $(objs) Makefile
	g++ -o $@ $(objs) $(libs) -O3 -g0 -s

%.cpp.o : %.cpp $(wildcard *.hpp) Makefile
	g++ -c -o $@ $< -std=c++11 


clean :
	- rm $(wildcard *.o askshialabeouf* ) 
