#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <mutex>
#include <random>

class RequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory
{
    std::vector<std::string> answers_;
    std::mutex answers_mutex_;
    std::default_random_engine rng_;
    std::uniform_int_distribution<std::size_t> r_;
    Poco::Net::HTTPRequestHandler * createRequestHandler( const Poco::Net::HTTPServerRequest & request ) override;
    std::string Answer();
public:
    RequestHandlerFactory();
};

class RequestHandler : public Poco::Net::HTTPRequestHandler
{
    void handleRequest( Poco::Net::HTTPServerRequest & request, Poco::Net::HTTPServerResponse & response ) override;
public:
    std::string answer_;
};
